#!/bin/bash

cat product/app/EmojiKeyboard/EmojiKeyboard.apk.* 2>/dev/null >> product/app/EmojiKeyboard/EmojiKeyboard.apk
rm -f product/app/EmojiKeyboard/EmojiKeyboard.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/priv-app/TranAodApk/TranAodApk.apk.* 2>/dev/null >> system_ext/priv-app/TranAodApk/TranAodApk.apk
rm -f system_ext/priv-app/TranAodApk/TranAodApk.apk.* 2>/dev/null
cat system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null >> system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk
rm -f system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null
cat system_ext/app/FaceID/FaceID.apk.* 2>/dev/null >> system_ext/app/FaceID/FaceID.apk
rm -f system_ext/app/FaceID/FaceID.apk.* 2>/dev/null
cat system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null >> system_ext/app/TranssionCamera/TranssionCamera.apk
rm -f system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null
cat system_ext/app/SmartAssistant/SmartAssistant.apk.* 2>/dev/null >> system_ext/app/SmartAssistant/SmartAssistant.apk
rm -f system_ext/app/SmartAssistant/SmartAssistant.apk.* 2>/dev/null
cat system_ext/app/AiGallery/AiGallery.apk.* 2>/dev/null >> system_ext/app/AiGallery/AiGallery.apk
rm -f system_ext/app/AiGallery/AiGallery.apk.* 2>/dev/null
cat system/system/apex/com.android.btservices.apex.* 2>/dev/null >> system/system/apex/com.android.btservices.apex
rm -f system/system/apex/com.android.btservices.apex.* 2>/dev/null
cat system/system/app/MiraVision/MiraVision.apk.* 2>/dev/null >> system/system/app/MiraVision/MiraVision.apk
rm -f system/system/app/MiraVision/MiraVision.apk.* 2>/dev/null
cat vendor/lib64/libst_sr_models.so.* 2>/dev/null >> vendor/lib64/libst_sr_models.so
rm -f vendor/lib64/libst_sr_models.so.* 2>/dev/null
